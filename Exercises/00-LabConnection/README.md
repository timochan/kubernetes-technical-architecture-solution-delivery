# Kubernetes - Technical Architecture Solution Delivery: Connect to the Labs
# Hands-on Exercises #00

### Objective



### Parts

[Part 1: Shell Access](Part-01-ShellAccess.md)

[Part 2: Rancher Access](Part-02-RancherAccess.md)

[Part 3: DownloadLabContent](Part-03-DownloadLabContent.md)

Return to the course [Table of Content](../README.md)
