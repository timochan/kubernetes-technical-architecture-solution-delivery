# Exercise #00 : Part 2
## Rancher Access

---

### Intro

In this workshop we will be using a Rancher instance as an additional tool to explore Kubernetes with.

---

### Rancher Access

The class instructor will provide your login username and password needed for Rancher.  You can connect to the rancher instance at the following address.

[https://rancher.agilebrainslabs.com](https://rancher.agilebrainslabs.com)

Currently there is not much yet to do in this Rancher instance but as you work through the Labs additional views and learning can be realized through this Rancher interface.

---

This concludes part 2 of Hands-on Exercise #00.  Continue on to Part 3 next.

[Part 3: Download Lab Content](Part-03-DownloadLabContent.md)
