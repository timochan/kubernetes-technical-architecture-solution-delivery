# Exercise #05 : Part 2
## Probes

---

### Intro

Probes can be used to validate theh functionality of a pod before traffic is sent to the pod and perodically on running pods.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/05-DeploymentControl
~~~

### Probes

To see probes in action let first create on that will always fail.

_probes-nginx-fail.yaml_
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
          name: web
        readinessProbe:
          initialDelaySeconds: 1
          periodSeconds: 2
          timeoutSeconds: 1
          successThreshold: 1
          failureThreshold: 1
          httpGet:
            host:
            scheme: HTTP
            path: /
            httpHeaders:
            - name: Host
              value: webapp
            port: 8000
```

Notice that we will be using a "readinessProbe" of the "httpGet" type.  The reason it will never be ready is that the probe is set to test port 8000 instead of port 80 that nginx is listening on.

Apply the manifest

```bash
kubectl apply -f probes-nginx-fail.yaml
```

Give it a few seconds then "Describe" the pod.

```bash
kubectl get all
```

Notice that the pods are "STATUS" = "Running" but are not "READY" (0 of 1 are ready).  "Describe" one of the pods to determine why it is note "READY".

```bash
kubectl describe pod/web-{POD-ID}
```

At the end you will see the "Events:" section.  The last event indicates the issue.

---

Now let us fix the deployment.

_probe-nginx.yaml_
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: web
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:alpine
        ports:
        - containerPort: 80
          name: web
        readinessProbe:
          initialDelaySeconds: 1
          periodSeconds: 2
          timeoutSeconds: 1
          successThreshold: 1
          failureThreshold: 1
          httpGet:
            host:
            scheme: HTTP
            path: /
            httpHeaders:
            - name: Host
              value: webapp
            port: 80
```

```bash
kubectl apply -f probe-nginx.yaml
```

Now verify that the Pods are Running and Ready.

---

Remove the deployment to cleanup when you are finished

---

This concludes part 2 of Hands-on Exercise #05.  Continue on to Part 3 next.

[Part 3: Holizontal PodAutoscalers](Part-03-HorizontalPodAutoscalers.md)
