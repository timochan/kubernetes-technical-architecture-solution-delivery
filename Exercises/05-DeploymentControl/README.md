# Kubernetes - Technical Architecture Solution Delivery: Deployment Control
# Hands-on Exercises #05

### Objective



### Parts

[Part 1: Jobs](Part-01-Jobs.md)

[Part 2: Probes](Part-02-Probes.md)

[Part 3: HorizontalPodAutoscalers](Part-03-HorizontalPodAutoscalers.md)


Return to the course [Table of Content](../README.md)
