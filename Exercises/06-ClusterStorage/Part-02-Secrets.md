# Exercise #06 : Part 2
## Secrets

---

### Intro

Secrets provide the same functionality as ConfigMaps with one minor difference.  Secrets are meant to be use with datasets that should be protected.  Kubernetes encodes the secret data as base64.  Base64 is used not because of it is extremely secure but because it is reversible without an additional passphrase or key. Kubernetes does make sure that base64 value is careful not exposed outside of people or processes that are granted access to it.  Here we will explore secrets.

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/06-ClusterStorage
~~~

### Secrets

We can create a secret in a namespace directly from the command line with the "--from-literal" option followed by the Key=Value pair for the secret.

```bash
kubectl create secret generic dev-secret --from-literal=username=admin --from-literal=password=’passw0rd123’
```

We can display the secret in YAML format with this command.  Notice that Kubernetes has encoded our literal values.

```bash
kubectl get secret dev-secret -o yaml
```

We can also create secrets from existing files.  We will do that here by creating a 'username.txt' and 'password.txt' file and adding those to a new secret named 'get-cred'

```bash
echo  -n ‘admin’ > ./username.txt
echo  -n ‘pa55w0rd’ > ./password.txt
kubectl create secret generic get-cred --from-file=./username.txt --from-file=./password.txt
```

Again we can output the new secret object as YAML and see how it encodes the content of the files.

```bash
kubectl get secrets get-cred -o yaml
```

We can create secrets from YAML files.  We have a file 'secret-app-creds.yaml' with values that have been encoded as base64.  Following is the content of that manifest.

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: app-creds
type: Opaque
data:
  password: MTIzNAo=
stringData:
  username: JohnSmith
```

Let us apply that secret to the "default" namespace.

```bash
kubectl apply -f secret-app-creds.yaml
```

Again we see the encoded value for the "password".  The "username" was also encoded as it was "stringData" but is stored in a Kubernetes secret.

```bash
kubectl get secrets app-creds -o yaml
```

Inside the pod the secrets are decoded for the processes.  Following is the 'pod-app-creds-secret.yaml' manifest.  Notice how it exposes the secret through a mounted volume to '/mnt/foo' in the container of the pod.

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-app-pod
  labels:
    app: my-app
spec:
  containers:
  - name: ubuntu
    image: project42/s6-ubuntu
    resources:
      limits:
        memory: "128Mi"
        cpu: "500m"
    volumeMounts:
    - name: foo
      mountPath: "/mnt/foo"
      readOnly: true
  volumes:
  - name: foo
    secret:
      secretName: app-creds
```

Go ahead and create that pod.

```bash
kubectl create -f pod-app-creds-secret.yaml
```

We can see the pod is running this time taking advantage of the label on the pod.

```bash
kubectl get pods -l app=my-app
```

Let's connect to a "bash" shell inside the lone container of the "my-app-pod" to see how the secrets can be exposed to processes inside the pod.

```bash
kubectl exec -it my-app-pod -- /bin/bash
```

We can navigate to the "/mnt/foo" directory and read the two files located there.

```bash
ls /mnt/foo
cat /mnt/foo/username && echo ''
cat /mnt/foo/password
```

Now see if you can remove the pod and the secrets used in this exercise with 'kubectl delete'.

---

This concludes part 2 of Hands-on Exercise #06.  Continue on to Part 3 next.

[Part 3: Volumes](Part-03-Volumes.md)
