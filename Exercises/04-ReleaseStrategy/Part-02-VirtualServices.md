# Exercise #04 : Part 2
## Virtual Services

---

### Intro

"Configuration affecting traffic routing. Here are a few terms useful to define in the context of traffic routing."

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/04-ReleaseStrategy
~~~

### Virtual Services

To test Istio "VirtualServices" we really need to render a webpage in a browser.  The simplest way to do that would be to take advantage of the Public IP of the primary lab instance combined with the "servicelb" provided by K3s as a LoadBalancer provider.  If you still have K3s running on your primary Lab instance, great let's us it.  If not please [install it from the labs](../02-K8sDistributions/Part-02-K3s.md).  You may need to remove an existing Kubernetes cluster first.  The end goal is to have K3s installed on the primary lab instance then import it into Rancher, once in Rancher add Istio.  Adding it to Rancher and installing Istio steps will follow.

Once you get K3s installed on your primary Lab instance your kubectl config will have been changed to point to the single node cluster.  You can verify that with 'kubectl get nodes'.

```bash
kubectl get nodes
```

Let us store this config in a separate file to make it simpler to go back and forth between K8s clusters.  There is a "kubectl config" option that allows different "context" to be stored in a single config file, but we will not be covering that here.  For now we will just swap configs as needed.

__Verify that your "kubectl" is currently configured for your single node K3s cluster__

The following command should return a single node, your primary node.

```bash
kubectl get nodes
```

If everything is correct we can backup the K3s config to `~/.kube/config-k3s`

```bash
cp $HOME/.kube/config $HOME/.kube/config-k3s
chmod 600 ~/.kube/config-k3s
```

---

##### Add your k3s cluster to Rancher

Add your K3s cluster to Rancher. From the Rancher UI:

1.	From the "Home" Dashboard, click "Import Existing" in the Cluster section.

1.	Choose "Generic", to import any K8s Cluster.

1.	Enter a Cluster Name including your name to easily identify it as your k3s cluster.

1.	Click Create.

1.	Copy the kubectl command to your clipboard and run it on a node where kubeconfig is configured to point to the cluster you want to import. If you are unsure it is configured correctly, run kubectl get nodes to verify before running the command shown in Rancher.

1.	If you are using self signed certificates, you will receive the message certificate signed by unknown authority. To work around this validation, copy the command starting with curl displayed in Rancher to your clipboard. Then run the command on a node where kubeconfig is configured to point to the cluster you want to import.

1.	When you finish you will see your cluster listed as "Active"

1.  To access the cluster dashboard select the three horizontal lines on the top left of the page, then select your cluster.

---

##### Istio

Follow the [instructions](Part-01-Istio.md#istio-1) on the previous lab to install Istio on your K3s cluster via the Rancher interface.  Again select only "Pilot" and "Ingress Gateway".  Don't forget to adjust the Resource limits as it will fail without those adjustments.

Run this command to allow the istio-ingressgateway to use the internal IP of your Lab instance.

```bash
kubectl -n istio-system patch service istio-ingressgateway --patch '{"spec":{"type":"LoadBalancer"}}'
```

---

We will use the Bookshelf app to demo the virtual services in Istio.  We first need a namespace.

```bash
kubectl create namespace bookinfo
kubectl label namespace bookinfo istio-injection=enabled
```

The "envoy" proxy should now be set to be injected.

```bash
kubectl get namespace -L istio-injection
```

```bash
kubectl -n bookinfo apply -f bookinfo.yaml
```

We can watch the app deploy.

```bash
watch kubectl -n bookinfo get all
```

Here is the first VirtualService defined in the 'bookinfo-gateway.yaml' file.  Notice how the VirtualService matches the routing for traffic comming in.

_bookinfo-gateway.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: Gateway
metadata:
  name: bookinfo-gateway
spec:
  selector:
    istio: ingressgateway # use istio default controller
  servers:
  - port:
      number: 80
      name: http
      protocol: HTTP
    hosts:
    - "*"
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: bookinfo
spec:
  hosts:
  - "*"
  gateways:
  - bookinfo-gateway
  http:
  - match:
    - uri:
        exact: /productpage
    - uri:
        prefix: /static
    - uri:
        exact: /login
    - uri:
        exact: /logout
    - uri:
        prefix: /api/v1/products
    route:
    - destination:
        host: productpage
        port:
          number: 9080
```

Let us apply that 'bookinfo-gateway.yaml' manifest

```bash
kubectl -n bookinfo apply -f bookinfo-gateway.yaml
```

We can list the gateway object

```bash
kubectl -n bookinfo get gateway
```

and the VirtualService

```bash
kubectl -n bookinfo get virtualservices
```

#### Connect to http://(PublicIP)/productpage

Refresh and notice the ratings

---

Now we can explore some more options.

_virtual-service-all-v1.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: productpage
spec:
  hosts:
  - productpage
  http:
  - route:
    - destination:
        host: productpage
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
  - reviews
  http:
  - route:
    - destination:
        host: reviews
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
  - ratings
  http:
  - route:
    - destination:
        host: ratings
        subset: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: details
spec:
  hosts:
  - details
  http:
  - route:
    - destination:
        host: details
        subset: v1
---
```

```bash
kubectl -n bookinfo apply -f virtual-service-all-v1.yaml
```

```bash
kubectl -n bookinfo get virtualservices -o yaml | less
```

_destination-rule-all.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: productpage
spec:
  host: productpage
  subsets:
  - name: v1
    labels:
      version: v1
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: reviews
spec:
  host: reviews
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
  - name: v3
    labels:
      version: v3
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: ratings
spec:
  host: ratings
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
  - name: v2-mysql
    labels:
      version: v2-mysql
  - name: v2-mysql-vm
    labels:
      version: v2-mysql-vm
---
apiVersion: networking.istio.io/v1alpha3
kind: DestinationRule
metadata:
  name: details
spec:
  host: details
  subsets:
  - name: v1
    labels:
      version: v1
  - name: v2
    labels:
      version: v2
---
```

```bash
kubectl -n bookinfo apply -f destination-rule-all.yaml
```

```bash
kubectl -n bookinfo get destinationrules -o yaml | less
```

#### Connect to http://{PublicIP}/productpage
Refresh and notice the ratings, all sent to "v1"

---

_virtual-service-reviews-test-v2.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: reviews
spec:
  hosts:
    - reviews
  http:
  - match:
    - headers:
        end-user:
          exact: jason
    route:
    - destination:
        host: reviews
        subset: v2
  - route:
    - destination:
        host: reviews
        subset: v1
```

```bash
kubectl -n bookinfo apply -f virtual-service-reviews-test-v2.yaml
```

```bash
kubectl -n bookinfo get virtualservice reviews -o yaml | less
```

#### Connect to http://{PublicIP}/productpage

- signin with jason:password

- signout and backin with jack:password

Notice the difference.  Now look back on the VirtualService above.

---

_virtual-service-ratings-test-delay.yaml_
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: VirtualService
metadata:
  name: ratings
spec:
  hosts:
  - ratings
  http:
  - match:
    - headers:
        end-user:
          exact: jason
    fault:
      delay:
        percentage:
          value: 100.0
        fixedDelay: 7s
    route:
    - destination:
        host: ratings
        subset: v1
  - route:
    - destination:
        host: ratings
        subset: v1
```

```bash
kubectl -n bookinfo apply -f virtual-service-ratings-test-delay.yaml
```

```bash
kubectl -n bookinfo get virtualservice ratings -o yaml
```

#### Connect to http://{PublicIP}/productpage

- sign in as jason:password

- notice the delay / timeout / error

Again look back at the above "VirtualService"

Let us remove the VirtualService with a delay.

```bash
kubectl -n bookinfo delete -f virtual-service-ratings-test-delay.yaml
```

And Set it back to "v1"

```bash
kubectl -n bookinfo apply -f virtual-service-all-v1.yaml
```

- notice everything is routed to "v1"

---

This concludes part 2 of Hands-on Exercise #04.  Continue on to Part 3 next.

[Part 3: Canary](Part-03-Canary.md)
