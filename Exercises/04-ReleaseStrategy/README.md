# Kubernetes - Technical Architecture Solution Delivery: Release Strategy
# Hands-on Exercises #04

### Objective



### Parts

[Part 1: Istio](Part-01-Istio.md)

[Part 2: VirtualServices](Part-02-VirtualServices.md)

[Part 3: Canary](Part-03-Canary.md)


Return to the course [Table of Content](../README.md)
