# Exercise #04 : Part 1
## Istio

---

### Intro

"Simplify observability, traffic management, security, and policy with the leading service mesh."

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/04-ReleaseStrategy
~~~

### Istio

Let us use the Rancher UI to install Istio.

You can go to the cluster tool options which is where we will find some of the common add-ons to K8s cluster that Rancher can help us install. Selecting the "Cluster Tools" in the bottom left.

![Rancher Cluster Tools](images/04-01-01.png "Rancher Cluster Tools")

Then select Istio from the list.

![Rancher Istio](images/04-01-02.png "Rancher Istio")

Set it to install into the "System" Project.

![System Project](images/04-01-03.png "System Project")

After clicking "Next" you will be presented with the "Components" section.  We only need "Enabled Pilot" and "Enabled Ingress Gateway" selected.  The other options are addons that take to many resources for our small cluster.

![Istio Components](images/04-01-04.png "Istio Components")

Click the "Install" button.  Rancher now install Istio into the RKE2 Cluster. Rancher now will use 'helm' to install Istio to your cluster.  After a minute or so.  Run this command to see all the Istio components.

```bash
kubectl get all -n istio-system
```

There is one think that we should change from the defaults and it is easier for now to do it utilizing the Rancher UI.  The helm chart for Istio that Rancher deploys has the 'istiod' service reserve "2048MB" of memory which is fine on a large cluster but that is a significant amount on our existing cluster.  It possibly could even block the deployment.  To change it to something more reasonable follow these steps.

- Within the Rancher UI select your cluster

- Change the "namespace" selector near the top center of the screen to "istio-system"

- Expend the "Workload" section on the left

- Select the "Deployments" page

- On this page you should see two deployments.  The one we want to adjust is 'istiod'.  select the three dots on the far right to "Edit Config".

- On the 'istiod' Deployment config page switch from "Deployment" to "Containers"

- Locate the "Limits and Reservations" section

- Change the "CPU Reservation" from "500" to "100"

- Change the "Memory Reservation" from '2048' to '256'

- Click on the "Save" button

- Now expand the "Storage" section on the left

- Select the "ConfigMaps" section

- "Edit Config" of the "istio-sidecar-injector"

- In the "Values" box scroll down to the first "resources" section

- Change limits -> cpu to "200mCPUs" (from 2000mCPUs)

- Change limits -> memory to "256MiB" (from 1024MiB)

- Change requests -> cpu to "20m" (from 100m)

- Change requests -> memory to "64Mi" (from 128Mi)

![Istio Limits](images/04-01-05.png "Istio Limits0")

- Click on the "Save" button

---

## Test out Istio

Istio uses the a label "istio-injection" to determine what pods will have the "envoy" proxy injected.  We can view that label value by adding a colomn for it with 'kubectl get namespaces'.

```bash
kubectl get namespace -L istio-injection
```

Create a few namespaces to work with.

```bash
kubectl create namespace servicemesh
kubectl create namespace no-servicemesh
```

Add the appropriate label to the "servicemesh" namespace.

```bash
kubectl label namespace servicemesh istio-injection=enabled
```

Now notice the label when listing "namespaces"

```bash
kubectl get namespace -L istio-injection
```

Let us install a simple sleep service into the "no-servicemesh" namespace.

```bash
kubectl -n no-servicemesh apply -f sleep.yaml
```

Now observe the resulting pod.  Notice that in the "READY" colomn there is "1/1".  That is 1 container is READY of the expected 1 containers in that pod.  By this we know that no "sidecart" container was injected.

```bash
kubectl -n no-servicemesh get pods
```

We could also "describe" the container to get the same information.

```bash
kubectl -n no-servicemesh describe pod sleep-<POD-ID>
```

Now apply the same sleep.yaml manifest this time to the "servicemesh" namespace.

```bash
kubectl -n servicemesh apply -f sleep.yaml
```

And list the pods, the "READY" column should be 2/2.

```bash
kubectl -n servicemesh get pods
```

notice the "sidecart" in the description of the pod.

```bash
kubectl -n servicemesh describe pod sleep-<POD-ID>
```

To remove the label from a namespace append a "-" to the end of it.

```bash
kubectl label namespace servicemesh istio-injection-
```

Verify that the label has been removed

```bash
kubectl get namespace -L istio-injection
```

You will notice that there still is a "sidecart" container in the pods.  This is because the "injection" happens only when a pod is scheduled.  Existing pods stay as is.  The "sidecart" is not part of the "PodSpec".

```bash
kubectl -n servicemesh get pods
```

We can simply remove the namespaces to remove the resounces in those namespaces and cleanup.

```bash
kubectl delete namespace no-servicemesh
kubectl delete namespace servicemesh
```

---

This concludes part 1 of Hands-on Exercise #04.  Continue on to Part 2 next.

[Part 2: VirtualServices](Part-02-VirtualServices.md)
