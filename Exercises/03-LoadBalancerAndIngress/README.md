# Kubernetes - Technical Architecture Solution Delivery: LoadBalancer and Ingress
# Hands-on Exercises #03

### Objective



### Parts

[Part 1: Services](Part-01-Services.md)

[Part 2: ClusterIP](Part-02-ClusterIP.md)

[Part 3: NodePort](Part-03-NodePort.md)

[Part 4: LoadBalancer](Part-04-LoadBalancer.md)

[Part 5: Ingress](Part-05-Ingress.md)

Return to the course [Table of Content](../README.md)
