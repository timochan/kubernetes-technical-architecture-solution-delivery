# Exercise #03 : Part 4
## LoadBalancer

---

### Intro

Within Kubernetes NodePorts are required to forward traffic from external clients to internal Service ClusterIPs. But they are far from ideal, NodePorts require your external clients to know and to have access to your node external IP addresses.  Over the lifecycle of your Kubernetes cluster your nodes may change or even fail which means the IP of those nodes may also change.  Not only the issues with using node IPs, NodePort also have the issue of not being ideal ports for the specific service provided.  The actual port of NodePort typically are from 30000 to 32767.  Those are not the ports we want to use for our externalized services.  Fortunately Kubernetes has a solution that provide a dedicated External IP address that can use the desired TCP or UDP port defined within Kubernetes Services within the Service type 'LoadBalancer'.  Services that have type LoadBalancer also have ClusterIPs and NodePorts.  The caveat is that IP addresses outside the Kubernetes cluster and the network they are connected to are by definition outside scope of Kubernetes.  Something is needed with the external IP addresses and the capability of utilizing those IP addresses, load-balancing traffic to those external IP addresses to NodePorts.  That requires an external load-balancer that is interfaced to Kubernetes.  Once that external load-balancer is in places and configured all that functionality can be abstracted by Kubernetes with the Services type LoadBalancer.

---

### LoadBalancer

Now that we have seen both the 'ClusterIP' and the 'NodePort' type services let us explore the third and final type the 'LoadBalancer'.  One of the advantages of uses a Kubernetes as a Service type cluster like AKS is that there already is a LoadBalancer provider configured for the cluster.  No additional configuration is needed to take advantage of the Service type 'LoadBalancer'.

In the previous exercise we demonstrated how to modify the service type using 'kubectl patch'.  Now see if you con do the same this time changing it to a 'LoadBalancer' type.  Once completed quickly check the Service with:

```bash
watch kubectl get svc -l app=nginx
```

You will notice that the 'watch' command reloads the 'kubectl get' command every 2 seconds.  Notice that the "EXTERNAL-IP" changes first to "<pending>" then to a private IP address from the MetalLB address pool.  In our instances that IP address is still a private IP address only availbile from inside the cluster. So we will have to connect to it from a shell on one of the cluster nodes.

```bash
`curl http://{Services_EXTERNAL-IP}`
```

Once finished testing and exploring see if you can clean-up all the pods and services in your default Namespace. Leave the 'jumpbox' as you will use it later. You will have to remove both the Deployments and the Services individually as they are decoupled from each other even though they use the same selector.

---

### Additional Service and LoadBalancer Exercises

##### Create a deployment and explore the exposed service

~~~bash
kubectl create deployment test --image=adongy/hostname-docker --port=3000
kubectl get deploy
kubectl get svc
kubectl expose deploy/test --port=80 --target-port=3000
kubectl get svc
~~~

Verify through curl from a shell on the 'jumpbox'.  The commands where covered earlier.

Set type to NodePort this time using 'kubectl edit', it will drop you into a 'vim' editor.

~~~bash
kubectl edit svc/test
kubectl get svc/test
~~~

Set type to LoadBalancer

~~~bash
kubectl edit svc/test
kubectl get svc/test
~~~

Verify through curl from a shell on one of the cluster nodes.

~~~bash
curl http://{Service_EXTERNAL-IP}
~~~

scale the deployment, remember 'kubectl' should always be run from your initial Lab instance where it was configured.

~~~bash
kubectl scale deploy test --replicas=3
~~~

Reload the site a few times, notice the change in hostname


##### Add another Service

~~~bash
kubectl create deployment nginx --image=nginx:alpine --port=80
kubectl get deploy/nginx
~~~

Expose the nginx service as __type=LoadBalancer__

~~~bash
kubectl expose deploy/nginx --port=80 --type=LoadBalancer
kubect	get svc/nginx
~~~

See if you can now now connect to the nginx service from a cluster node.

##### Clean-up

~~~bash
kubectl delete deploy/test
kubectl delete deploy/nginx
~~~

Notice that the deployment is removed but the service remains

~~~bash
kubectl get svc,deploy
~~~

Remove the services

~~~bash
kubectl delete svc/test
kubectl delete svc/nginx
~~~

Now the services are also removed.  delete does not cascade between deployments and services.

~~~bash
kubectl get svc,deploy
~~~

---

This concludes part 4 of Hands-on Exercise #03.  Continue on to Part 5 next.

[Part 5: Ingress](Part-05-Ingress.md)
