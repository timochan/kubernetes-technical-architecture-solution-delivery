# Exercise #01 : Part 2
## skopeo

---

### Intro

In this exercise we will use skopeo to gather information from remote cantainer images.

---

### skopeo

We first need to install skopeo.

```bash
sudo apt update
sudo NEEDRESTART_SUSPEND=a apt install -y skopeo
```

As before there is an online help.  Look through this to get an idea of what is availible.

```bash
skopeo --help
```

Let us explore that upstreame fedora image what we used in the last exercise.  We can do that with the 'skopeo inspect' command.

```bash
skopeo inspect docker://registry.fedoraproject.org/fedora:latest | jq
```

We can return ojects from the inspect command a few different ways.  We installed the 'jq' command we can use it to filter a JSON section.

```bash
skopeo inspect docker://registry.fedoraproject.org/fedora:latest | jq '.Digest'
```

We also can ruturn one item like the ".RepoTags" using the '--format' option.  Let us look at the "alpine:latest" image on Dockerhub.

```bash
skopeo inspect --format "{{ .RepoTags }}" docker://docker.io/library/alpine
```

For instance we can find the Architecture of an image.  This is on the "python:latest" image on Dockerhub.

```bash
skopeo inspect --format "{{ .Architecture }}" docker://docker.io/library/python
```

We can also explore images of different architetures with an "--override" option.  Since it is architecture we are exploring we will use "--override-arch".

```bash
skopeo inspect --format "{{ .Architecture }}" --override-arch "arm64" docker://docker.io/library/alpine
```

On your own see if you can find the availible tags (RepoTags) for the official "busybox" image on Dockerhub using "skopeo".  Compare them to the list found on the [DockerHub interface](https://hub.docker.com/_/busybox/tags).

---

This concludes part 2 of Hands-on Exercise #01.  Continue on to Part 3 next.

[Part 3: podman](Part-03-podman.md)
