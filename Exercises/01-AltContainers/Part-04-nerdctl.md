# Exercise #01 : Part 4
## nerdctl

---

### Intro

Nerdctl is a direct replacement to Docker that supports more bleading edge features found in the Moby project and containerd.  You should choose either Docker, Nerdctl, or Podman/buildah/skopeo to use on one system.

---

### nerdctl

Install some tools we will use later.

```bash
sudo apt update
sudo NEEDRESTART_SUSPEND=a apt install -y  tree
```

We will set some environment variables to help with the installs.

```bash
export ARCH=amd64
export CNI_VERSION=1.1.1
export BUILDKIT_VERSION=0.10.3
export NERDCTL_VERSION=0.20.0
```

##### Install CNI

We need to install a version of Container Network Interface (CNI).  We will install it in the '/opt/cni/bin' directory.  Create it first.

```bash
sudo mkdir -p /opt/cni/bin
```

Now we can install CNI with this command.

```bash
curl -fsSL https://github.com/containernetworking/plugins/releases/download/v${CNI_VERSION}/cni-plugins-linux-${ARCH}-v${CNI_VERSION}.tgz | sudo tar xzvvC /opt/cni/bin
```

We can view what was installed in that directory with this command.

```bash
tree /opt/cni/bin
```

##### Install BuildKit

BuildKit is used to build images.  We can install it here.

```bash
curl -fsSL https://github.com/moby/buildkit/releases/download/v${BUILDKIT_VERSION}/buildkit-v${BUILDKIT_VERSION}.linux-${ARCH}.tar.gz | sudo tar xzvvC /usr/local
```

And again see what was instaled, this time in the '/usr/local/bin' directory.

```bash
tree /usr/local/bin
```

We need some systemd services for BuildKit.  We will install them next.

```bash
sudo curl -fsSL https://raw.githubusercontent.com/moby/buildkit/master/examples/systemd/system/buildkit.service -o /etc/systemd/system/buildkit.service
sudo curl -fsSL https://raw.githubusercontent.com/moby/buildkit/master/examples/systemd/system/buildkit.socket -o /etc/systemd/system/buildkit.socket
```

And enable and start the service.

```bash
sudo systemctl daemon-reload
sudo systemctl enable buildkit
sudo systemctl start buildkit
```

Let us verify that the service is started.

```bash
systemctl status --no-pager buildkit
```

##### Install containerd and rootlesskit

Containerd is needed for nerdctl.  So let install in from the Ubuntu pacakges.   We also will install rootlesskit to allow rootless containers with nerdctl.

```bash
sudo apt update
sudo NEEDRESTART_SUSPEND=a apt install -y  containerd rootlesskit
```

##### Intall nerdctl

Now that we have all the dependancies in place we can install nerdctl.

```bash
curl -fsSL https://github.com/containerd/nerdctl/releases/download/v${NERDCTL_VERSION}/nerdctl-${NERDCTL_VERSION}-linux-${ARCH}.tar.gz | sudo tar xzvvC /usr/local/bin
```

Notice the binaries that it added.

```bash
tree /usr/local/bin
```

If we want to run containers in rootless mode we will need to enable some "--user" systemd services.  We will use a supplied script to accomplish this task.

```bash
containerd-rootless-setuptool.sh install
```

We can verify that it is running.

```bash
systemctl --user status --no-pager containerd.service
```

That same script can also setup rootless containers at build time with BuildKit.

```bash
containerd-rootless-setuptool.sh install-buildkit
```

Verify that service also.

```bash
systemctl --user status --no-pager buildkit.service
```

##### Run the image

Now let us actually run the `hello-world` image as a container.

~~~shell
nerdctl run hello-world
~~~

The `hello-world` image dumps a message to the shell and exits.  Notice the output from the container.

#### Something more ambitious

The output from `hello-world` indicated something more ambitious.  Go ahead and try it and explore a bit and see what you find.

How can you tell if you are on a shell inside a container?

---

All nerdctl commands follow a standard syntax similar to docker.  There are aliases to the standard commands that we consider sane shortcuts that exist for backwards compatibility with earlier versions of Docker.

~~~
nerdctl <context> <command> <target/arguments>
~~~

The context is usually stuff like `container`, `image`, or `network`. the command is with regards to the context given.

#### Examples

some examples would be

###### list running containers

~~~shell
nerdctl container ls
~~~

###### list all defined containers running or stopped

~~~shell
nerdctl container ls -a
~~~

###### list local images

~~~shell
nerdctl image ls
~~~

###### remove a unused local images

~~~shell
nerdctl image rm <imageID>
~~~

###### start a new `bash` shell inside an running container

~~~shell
nerdctl container exec -it <containerID> bash
~~~

###### list the hosts docker networks

~~~shell
nerdctl network ls
~~~

### Online help

You can always get online help regarding the `nerdctl` command.

~~~shell
nerdctl --help
~~~

The output lists the nerdctl "contexts" in the "Management Commands:" section.  You can get more specific help within a specific "context"

~~~shell
nerdctl <context> --help
~~~

For instance to find information regarding the `container` context:

~~~shell
nerdctl container --help
~~~

#### Explore

You now know the basics of the `nerdctl`.  Explore around, see what you can find.  You are not going to hurt it.

---

#### Basic build

Now create a directory to work from as you build Docker images.

~~~shell
mkdir -p ~/container/web-v1/html
~~~

Change to the directory and copy the artifacts that are needed from the `src` location.

~~~shell
cd ~/container/web-v1
cp ~/content/kubernetes-technical-architecture-solution-delivery/src/01-AltContainers/v1/Dockerfile .
cp ~/content/kubernetes-technical-architecture-solution-delivery/src/01-AltContainers/v1/index.html html/
~~~

Modify index.html found in the 'html' directory, replace the NAME string with your name.

Now you should be able to build a new image name `local/web:v1`.

~~~shell
nerdctl image build -t local/web:v1 .
~~~

After the build has complemented you should now see the new image in the hosts local image store.

~~~shell
nerdctl image ls
~~~

Let us see if the image succeeds by creating on new container based on the newly created image.

~~~shell
nerdctl container run -d -p 8000:80 --name static1 local/web:v1
~~~

The container is "published" on TCP port 8000 so we should be able to connect to it from a local browser. Don't forget to change the url to your hostname or IP.
###### Test with curl

```bash
curl http://localhost:8000
```

###### Test with your browser

Replace {PublicHostnameOrIP} with your PublicIP or Public Hostname

'http://{PublicHostnameOrIP}:8000'

### More complex docker file

That `Dockerfile` was very simple. Next you will create a bit more complex container be starting with a fresh `alpine:3.11` system, installing `lighttpd` and configuring it to start the process when the container is run.

Make some space to work on this new web-v2 `Dockerfile` and artifacts.

~~~shell
mkdir -p ~/container/web-v2/html
cd ~/container/web-v2
~~~

As before let us copy some artifacts, including the `Dockerfile` from the `src` directory for the labs.  

~~~shell
cp ~/content/kubernetes-technical-architecture-solution-delivery/src/01-AltContainers/v2/Dockerfile .
cp ~/content/kubernetes-technical-architecture-solution-delivery/src/01-AltContainers/v2/index.html html/
~~~

As Before modify the `html/index.html` file and replace `NAME` with your name.

Build the new image version of the `local/web` image.  This time with a different version tag `v2`.

~~~shell
nerdctl image build -t local/web:v2 .
~~~

As before let us start a new container listening on TCP port 80.  We will use the "publish" option from the nerdctl cli.  The "publish" option is "-p" followed by two port numbers that are colon delimited "hostPort:containerPort", so if you want to redirect port 8080 on the host to port 80 on the container you can use "-p 8080:80".

~~~shell
nerdctl container run -d -p 8001:80 --name static2 local/web:v2
~~~

Verify that the webpage loads with your new docker image built from `alpine:3.11`.

###### Test with curl

```bash
curl http://localhost:8001
```

###### Test with your browser

Replace {PublicHostnameOrIP} with your PublicIP or Public Hostname

'http://{PublicHostnameOrIP}:8001'


### On your own now

First look through the `Dockerfile` located at `~/container/web-v2/Dockerfile`, make sure you understand what each step is doing.  Once you have a decent understanding of what is going on, see if you can create a `local/web:v3` image that serves this webpage on TCP port 80.  As always replace the `NAME` section.

~~~html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Alpine 3.11 and lighttpd</title>
</head>
<body>
    custom image v3<br>
    NAME, Your Server is online
</body>
</html>
~~~

Then deploy the new `local/web:v3` image on a container named `static3` listening on TCP port 8002.  Once it is deployed verify that it works and go ahead and brag about it.  Share the link to your new sit with the class instructor.

---

#### Container Images

We have used the `nerdctl` interface a number of times already with `nerdctl image` context. For instance to view the images in the local image store you have run.

~~~shell
nerdctl image ls
~~~
More recently you have "built" new images based on `Dockerfiles` with `nerdctl build`. Another handy option with images which can help get an idea how an image was created showing each layer is the `nerdctl image history` command.

~~~shell
nerdctl image history local/web:v1
~~~

As with other resources you can use the `inspect` option to get the metadata associated with an image.

~~~shell
nerdctl image inspect local/web:v1
~~~

Notice the "GraphDriver" section.  In that section you can find the location on disk for each layer of an image.

One last image command that is useful is the ability to remove images from an images store.

~~~shell
nerdctl image rm hello-world:latest
~~~

---

#### Clean up

See if you can figure out how to remove all the defined containers and remove the images in the local image store?

Cleaning up will help us not have conflicts later.

---

This concludes part 1 of Hands-on Exercise #01.

Return to the [Exercise Page](../README.md)
