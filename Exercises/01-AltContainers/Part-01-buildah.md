# Exercise #01 : Part 1
## buildah

---

### Intro

Buildah is used to create OCI standard container images.

---

### buildah

Install the 'buildah' binary.  We also will install 'jq' to format JSON with.

```bash
sudo apt update
sudo NEEDRESTART_SUSPEND=a apt install -y buildah jq
```

Check the buildah version

```bash
buildah --version
```

Always be aware and use the online help for the tools.

```bash
buildah --help
```

List the container images with buildah.

```bash
buildah images
```

List the buildah containers.  There currently will not be any.  We will add some later.

```bash
buildah containers
```

Let us start a new container "FROM fedora" the upstream image.   Notice the similarities to 'Dockerfile' syntax.

```bash
buildah from fedora
```

List the containers again.  Now we have a container that we are working on based of of Fedora.

```bash
buildah containers
```

The images now include the upstream 'fedora' image as it is required by the working container.

```bash
buildah images
```

There is a running container named 'fedora-working-container',  We will connect to it using buildah.

```bash
buildah run fedora-working-container bash
```

Now your prompt has changed, notice the hostname is the same as the 'fedora-working-container' ID.

```bash
echo "File added to image via buildah shell" > /root/newfile.txt
```

When finished working in the container we can exit it.  The exit command does not stop and commit the changes to a new layer.  Buildah allows us to continue to make changes and choose when to commit our changes.

```bash
exit
```

Let us add a package to the working container.  We can add nginx by running the approriate command in the working container.

```bash
buildah run fedora-working-container dnf install -y nginx
```

We can also add local files to the container.  So let us first create a file that we will add.

```bash
echo "Hello World" > index.html
```

And copy that file to the working container.

```bash
buildah copy fedora-working-container index.html /usr/share/nginx/html/index.html
```

We can set meta data in the container layer with the 'buildah config' command.  Here we can set the "CMD" for the image.

```bash
buildah config --cmd '["nginx", "-g" ,"daemon off;"]' fedora-working-container
```

Once we have all our desired changes we can commit those changes to a new layer on top of the "fedora" image.  Creating a new image in our imagestor.

```bash
buildah commit fedora-working-container fedora-nginx
```

Now when we list images we see the new "fedora-nginx:latest" image.

```bash
buildah images
```

We can use buildah to inspect the images to explore the image details.

```bash
buildah inspect --type image fedora-nginx | jq
```

We can even return specific JSON objects from the image inspect with the format option.

```bash
buildah inspect --format '{{.OCIv1.Config.Cmd}}' fedora-nginx
````

The 'buildah inspect' is not limited to images it also works with the buildah containers.

```bash
buildah inspect --format '{{.OCIv1.Config.Cmd}}' fedora-working-container
```

See if you can create an additional working container, this time based on "ubuntu".  To that ubuntu container add the 'nginx' package and commit it as a new image named "ubuntu-nginx".

---

This concludes part 1 of Hands-on Exercise #01.  Continue on to Part 2 next.

[Part 2: skopeo](Part-02-skopeo.md)
