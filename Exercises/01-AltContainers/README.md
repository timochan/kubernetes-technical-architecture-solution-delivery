# Kubernetes - Technical Architecture Solution Delivery: Alternative Containers
# Hands-on Exercises #01

### Objective



### Parts

[Part 1: buildah](Part-01-buildah.md)

[Part 2: skopeo](Part-02-skopeo.md)

[Part 3: podman](Part-03-podman.md)

[Part 4: nerdctl](Part-04-nerdctl.md)

Return to the course [Table of Content](../README.md)
