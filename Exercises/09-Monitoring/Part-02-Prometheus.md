# Exercise #09 : Part 2
## Prometheus

---

### Intro

We will use Rancher to setup Prometheus and Grafana.

---

### Prometheus

We will again use Ranchers interface to Helm to install the Monitoring Tools which includes Prometheus and Grafana.  Selete the "Install Monitoring" option from the deshboard when you are on your RKE2 cluster.  You will find a link near the top right.

![Rancher Install Monitoring](images/09-02-01.png "Rancher Install Monitoring")

Then Select "Monitoring" from the list.

![Rancher Install Monitoring](images/09-02-02.png "Rancher Install Monitoring")

Select the "System" Project to install into.

![Monitoring System Project](images/09-02-03.png "Monitoring System Project")

Leave it on RKE2 type then select "Install".  You will see the monitoring tools installed.

---

Once installed you will see a new on the menu.  Navigate there and explore some of the monitoring.  It may take a few minutes for data to start filling in.  The Grafana link probably would be of most interest.

---

This concludes part 2 of Hands-on Exercise #09.

Return to the [Exercise Page](../README.md)
