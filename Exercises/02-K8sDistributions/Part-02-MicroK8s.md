# Exercise #02 : Part 2
## Microk8s

---

### Intro

"Zero-ops, pure-upstreame Kubernetes, from developer workstations to production"

---

### Microk8s

Install MicroK8s via snap.

```bash
sudo snap install microk8s --classic
```

Verify that it is installed.

```bash
sudo microk8s.status --wait-ready
```

Enable some MicroK8s plugins.

```bash
sudo microk8s.enable dns
sudo microk8s.enable hostpath-storage ingress metallb
```

We will be using the private IP address as a range (eth0) of our lab instance for MetalLB.  When prompted to "Enter each IP address range delimited by comma" your input should look something like '10.0.1.x/32'. To simplify this task the instructor should provide that IP address for you.   

Add your user to the 'microk8s' group to allow running the 'microk8s' commands without "sudo".

```bash
sudo usermod -a -G microk8s $USER
```

__Note: You must logout and log back in to get the local shell to recognize the newly added 'microk8s' group__

Let's verify the versions of MicroK8s
```bash
microk8s.kubectl version
```

We also can install 'kubectl' directly instead of using 'microk8s.kubectl'.

```bash
sudo snap install kubectl --classic
```

Let's add a configuration for 'kubectl' to use

```bash
mkdir -p $HOME/.kube
microk8s config > $HOME/.kube/config
```

And verify it works

```
kubectl version
kubectl get nodes
```

---

Now you can run through some Kubernetes exercise.

[Extra Exercises](ExtraExercises)

---

### Optional Clean-up (Skip this as we will be using MicroK8s on the this lab instance later later)

If you want to install a different Kubernetes distribution or if you just want to uninstall the  Microk8s cluster here are the commands to remove Microk8s.

Stop the cluster

```bash
sudo microk8s.stop
```

Remove kubectl

```bash
sudo snap remove kubectl
```

Remove the kubectl configs and caches.

```bash
rm -rvf ~/.kube
```

And finally remove the Microk8s cluster.

```bash
sudo snap remove microk8s
```

It would be good to logout and back in to help the shell not get confused about different versions of binaries like 'kubectl'.

```bash
exit
```

And log back into your lab instance to finish reseting the shell.

---

This concludes part 2 of Hands-on Exercise #02.  Continue on to Part 3 next.

[Part 3: RKE](Part-03-RKE.md)
