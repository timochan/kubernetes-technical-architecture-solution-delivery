# Exercise #02 : Part 3
## RKE

---

### Intro

"RKE2, also known as RKE Government, is Rancher's next-generation Kubernetes distribution."

"It is a fully conformant Kubernetes distribution that focuses on security and compliance within the U.S. Federal Government sector."

---

### RKE

We can split RKE into two different type of installs.  The first using the 'RKE' cli but that requires docker so we will skip this one for now.  The second and the one we will use we install 'RKE2' to two a pair of nodes (a & b) through the Rancher interface.

---

### Multi node RKE2 cluster

- In the Rancher web interface select "Create" on the "Clusters" page.

- Make sure that the toggel switch for RKE <-> "RKE2/K3s" is set on "RKE2/K3s"

![RKE2 switch](images/02-03-01.png "RKE2 switch")

- Then select "Custom" cluster type.  

![RKE2 Custom](images/02-03-02.png "RKE2 Custom")

- Set a Cluster Name including your name or username to identify it as your cluster.

![RKE2 Settings](images/02-03-03.png "RKE2 Settings")

- After a few seconds you will be presented with the remaining steps to complete your RKE2 Kubernetes cluster install. Copy the "Registration Command" and run it on your two additional Lab instances ("Lab#a" & "Lab#b")

![RKE2 Launch](images/02-03-04.png "RKE2 Launch")

- After a few minutes you should notice the "Cluster" has moved to "Active".  You can view the clusters through the Rancher interface by clicking on "Clusters" then selecting the "Explore" button next to your cluster.

- You also can access the cluster via shortcut to "EXPLORE CLUSTERS" by selecting the top left section of the page.

![Explore Clusters](images/02-03-05.png "Explore Clusters")

---

You now will have access to the Kubernetes Rancher Dashboard feel free to explore here and see how you can manage the Kubernetes cluster from the Rancher UI.

![Clusters Dashboard](images/02-03-06.png "Cluster Dashboard")

---

#### Setup kubectl to connect to your cluster

Run the following commands on your primary Lab instance.  You can use it to connect to your multi-node RKE2 Kubernetes cluster.

_add additional packages_
```bash
sudo NEEDRESTART_SUSPEND=a apt install -y bash-completion
```

If you do not already have it.  Insntall 'kubectl'

###### Install Kubectl

_Install kubectl_
```bash
sudo snap install kubectl --classic
```

###### Kubectl config

```bash
mkdir -p $HOME/.kube
touch $HOME/.kube/config-rke2
```

Now copy the the "KubeConfig" from the Rancher UI when exploring your Cluster.  Paste the content into '$HOME/.kube/config-rke2' with your editor of choice.  In the example bellow we are using 'vim' but feel free to use 'nano' or any other editor you choose.

![Clusters KubeConfig](images/02-03-07.png "Cluster KubeConfig")

```bash
vim $HOME/.kube/config-rke2
```

And copy it to the final location.  We will kee the "config-rke2" for safe keeping incase we need to restore it.

```bash
cp $HOME/.kube/config-rke2 $HOME/.kube/config
chmod 600 ~/.kube/config
chmod 600 ~/.kube/config-rke2
```

---

Now you should have access to the RKE2 Cluster from your primary Lab instance.

_Verify kubectl versions_
```bash
kubectl version
```

_Verify cluster with kubectl_
```bash
kubectl get nodes
```

---

Now you can run through some Kubernetes exercise.

[Extra Exercises](ExtraExercises)

---

This concludes part 3 of Hands-on Exercise #02.  Continue on to Part 4 next.

[Part 4: Rancher Desktop](Part-04-K3s-RancherDesktop.md)
