# Kubernetes - Technical Architecture Solution Delivery: K8s Distributions
# Hands-on Exercises #02

### Objective



### Parts

[Part 1: K3s](Part-01-K3s.md)

[Part 2: MicroK8s](Part-02-MicroK8s.md)

[Part 3: RKE](Part-03-RKE.md)

[Part 4: Rancher Desktop](Part-04-K3s-RancherDesktop.md)

[Extra: K8s Exercises](ExtraExercises) <- These can be run on any K8s cluster

[Part 5: Additional Tools](Part-05-AdditionalTools)

Return to the course [Table of Content](../README.md)
