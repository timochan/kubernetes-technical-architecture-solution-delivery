# Exercise #02 : Part 4
## K3s - Rancher Desktop

---

### Intro

"An open-source desktop application for Mac, Windows and Linux. Rancher Desktop runs Kubernetes and container management on your desktop. You can choose the version of Kubernetes you want to run. You can build, push, pull, and run container images using either containerd or Moby (dockerd). The container images you build can be run by Kubernetes immediately without the need for a registry."

---

### K3s - Rancher Desktop

Optionally if you want to have your own Kubernetes cluster running on your own machine in a simple small virtual machine Rancher Desktop is a great option.  Installation is different for different Operating Systems.  But instructions can be found at the Rancher Desktop web page.

[https://rancherdesktop.io](https://rancherdesktop.io)

You do not need to install this locally, it is your machine, but if you choose to it does provide a similar tool to what we will be using in the labs.

---

Now you can run through some Kubernetes exercise.

[Extra Exercises](ExtraExercises)

---

This concludes part 4 of Hands-on Exercise #02. Continue on to Part 5 next.

[Part 5: Additional Tools](Part-05-AdditionalTools.md)
