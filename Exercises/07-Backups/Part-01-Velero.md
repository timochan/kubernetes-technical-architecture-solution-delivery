# Exercise #07 : Part 1
## Velero

---

### Intro

Velero is an open source tool to safely backup and restore, perform disaster recovery, and migrate Kubernetes cluster resources and persistent volumes.

---

Before we start this exercise let us make sure we have the most recent version of the lab exercise content.

```bash
cd ~/content/kubernetes-technical-architecture-solution-delivery/
git pull
```

### Velero

We need to download and install the velero binary file.

```bash
cd ~/
wget https://github.com/vmware-tanzu/velero/releases/download/v1.9.0/velero-v1.9.0-linux-amd64.tar.gz
```

We can expand the tarball

```bash
tar -xzvf velero-v1.9.0-linux-amd64.tar.gz
```

Move the binary to '/usr/local/bin'

```bash
sudo mv velero-v1.9.0-linux-amd64/velero /usr/local/bin/
sudo chmod +x /usr/local/bin/velero
```

---

The manifest files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/07-Backups
~~~

##### Deploy minio to be a destination of the backups.

```bash
kubectl apply -f minio.yaml
```

Watch the deployment.  When minio is completely running continue on.  It is deployed in the "velero" namespace.

```bash
watch kubectl get all -n velero
```

##### Deploy velero to the Cluster

```bash
velero install --provider aws --plugins velero/velero-plugin-for-aws:v1.2.1 --bucket velero \
      --secret-file ./credentials-velero --use-volume-snapshots=false \
      --backup-location-config region=minio,s3ForcePathStyle="true",s3Url=http://minio.velero.svc:9000
```

Again watch to make sure everything is deployed.

```bash
watch kubectl get all -n velero
```

We could also verify with this command

```bash
kubectl get deployments -l component=velero --namespace=velero
```

---

##### Install an app to backup

Now we need some work load to backup.  We will install a nginx webapp.

```bash
kubectl apply -f nginx.yaml
```

You will notice that everything was installed into the  "nginx-example" namespace.  Let us look and see what is there.

```bash
kubectl get all,pvc,pv -n nginx-example
```

---

##### Backup the app

Now we will run a single backup.  Velero looks cluster wide for what to backup so we will use a "--selector" to match what objects to backup.

```bash
velero backup create nginx-backup --selector app=nginx
```

We can get a list of backups with this command.

```bash
velero backup get
```

We can describe a backup

```bash
velero backup describe nginx-backup
```

---

##### Disaster

Now let's simulate a disaster.  We will remove the namespace of the nginx app.

```bash
kubectl delete namespace nginx-example
```

Verify that it gone

```bash
kubectl get namespaces
```

---

We can restore from the backup

```bash
velero restore create --from-backup nginx-backup
```

We can list the restores

```bash
velero restore get
```

Once the restore "STATUS" is "COMPLETED" we can verify that it is back.

```bash
kubectl get all,pvc,pv -n nginx-example
```

---

##### Clean up Backups

We can remove the backup with this command.

```bash
velero backup delete nginx-backup
```

Verify that it is removed

```bash
velero backup get
```

---

##### Other options (No need to run just examples)

Alternatively if you want to backup all objects except those matching the label backup=ignore:

```bash
velero backup create nginx-backup --selector 'backup notin (ignore)'
```

Or to create regularly scheduled backups based on a cron expression using the app=nginx label selector:

```bash
velero schedule create nginx-daily --schedule="0 1 * * *" --selector app=nginx
```

Alternatively, you can use some non-standard shorthand cron expressions:

```bash
velero schedule create nginx-daily --schedule="@daily" --selector app=nginx
```

Backup just a specific namespace

```bash
velero backup create nginx-backup --include-namespaces nginx-example
```
---


This concludes part 1 of Hands-on Exercise #07.

Return to the [Exercise Page](../README.md)
