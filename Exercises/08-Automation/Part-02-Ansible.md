# Exercise #08 : Part 2
## Ansible

---

### Intro

"Ansible delivers simple IT automation that ends repetitive tasks and frees up DevOps teams for more strategic work."

---

### Ansible

##### Install Ansible

```bash
sudo apt update
sudo NEEDRESTART_SUSPEND=a apt install -y ansible
```

Let us also add the python3-kubernetes package

```bash
sudo NEEDRESTART_SUSPEND=a apt install -y python3-kubernetes
```

We can also install the Galaxy kubernetes collection

```bash
ansible-galaxy collection install kubernetes.core
```

Verify Ansible is installed

```bash
ansible --version
```

---

##### Kubernetes ansible support

- Use the Kubernetes Python client to perform CRUD operations on K8s objects.
- Pass the object definition from a source file or inline. See examples for reading files and using Jinja templates or vault-encrypted files.
- Access to the full range of K8s APIs.
- Use the kubernetes.core.k8s_info module to obtain a list of items about an object of type kind
- Authenticate using either a config file, certificates, password or token.
- Supports check mode.

---

##### Authentication

Ansible will need to authenticate to the Kubernetes cluster. By default the Kubernetes Rest Client will look for ~/.kube/config, and if found, connect using the active context. You can override the location of the file using the kubeconfig parameter, and the context, using the context parameter.

Basic authentication is also supported using the username and password options. You can override the URL using the host parameter. Certificate authentication works through the ssl_ca_cert, cert_file, and key_file parameters, and for token authentication, use the api_key parameter.

---

The ansible files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/08-Automation/ansible
~~~

---

In that directory you will find a few files.

The ansible project config file:

_ansible.cfg_
```
[defaults]
inventory = hosts
host_key_checking = False
```

The ansible project inventory file:

_hosts_
```
[local]
localhost
```

A ansible playbook including the tasks to run:

_nginx-deployment-playbook.yaml_
```yaml
---
- hosts: localhost
  gather_facts: false

  tasks:
    - name: Create a k8s namespace
      kubernetes.core.k8s:
        name: my-nginx
        api_version: v1
        kind: Namespace
        state: present
    - name: Create a Deployment from a manifest
      kubernetes.core.k8s:
        state: present
        src: "{{ playbook_dir }}/manifests/nginx-deployment.yaml"
    - name: Expose the Deployment with a service
      kubernetes.core.k8s:
        state: present
        src: "{{ playbook_dir }}/manifests/nginx-service.yaml"
...
```

There also are some kubernetes manifest files in the 'manifests' directory


Let us execute the task and see Ansible create the resources.

```bash
ansible-playbook nginx-deployment-playbook.yaml
```

We can verify the creation of the resources in the "my-nginx" namespace.

```bash
kubectl get all -n my-nginx
```

---

Here are some more example tasks that ansible can use with Kubernetes

```yaml
- name: Create a k8s namespace
  kubernetes.core.k8s:
    name: testing
    api_version: v1
    kind: Namespace
    state: present

- name: Create a Service object from an inline definition
  kubernetes.core.k8s:
    state: present
    definition:
      apiVersion: v1
      kind: Service
      metadata:
        name: web
        namespace: testing
        labels:
          app: galaxy
          service: web
      spec:
        selector:
          app: galaxy
          service: web
        ports:
        - protocol: TCP
          targetPort: 8000
          name: port-8000-tcp
          port: 8000

- name: Remove an existing Service object
  kubernetes.core.k8s:
    state: absent
    api_version: v1
    kind: Service
    namespace: testing
    name: web

# Passing the object definition from a file

- name: Create a Deployment by reading the definition from a local file
  kubernetes.core.k8s:
    state: present
    src: /testing/deployment.yml

- name: >-
    Read definition file from the Ansible controller file system.
    If the definition file has been encrypted with Ansible Vault it will automatically be decrypted.
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('file', '/testing/deployment.yml') | from_yaml }}"

- name: Read definition template file from the Ansible controller file system
  kubernetes.core.k8s:
    state: present
    template: '/testing/deployment.j2'

- name: Read definition template file from the Ansible controller file system that uses custom start/end strings
  kubernetes.core.k8s:
    state: present
    template:
      path: '/testing/deployment.j2'
      variable_start_string: '[['
      variable_end_string: ']]'

- name: Read multiple definition template file from the Ansible controller file system
  kubernetes.core.k8s:
    state: present
    template:
      - path: '/testing/deployment_one.j2'
      - path: '/testing/deployment_two.j2'
        variable_start_string: '[['
        variable_end_string: ']]'

- name: fail on validation errors
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('template', '/testing/deployment.yml') | from_yaml }}"
    validate:
      fail_on_error: yes

- name: warn on validation errors, check for unexpected properties
  kubernetes.core.k8s:
    state: present
    definition: "{{ lookup('template', '/testing/deployment.yml') | from_yaml }}"
    validate:
      fail_on_error: no
      strict: yes

# Download and apply manifest
- name: Download metrics-server manifest to the cluster.
  ansible.builtin.get_url:
    url: https://github.com/kubernetes-sigs/metrics-server/releases/latest/download/components.yaml
    dest: ~/metrics-server.yaml
    mode: '0664'

- name: Apply metrics-server manifest to the cluster.
  kubernetes.core.k8s:
    state: present
    src: ~/metrics-server.yaml

# Wait for a Deployment to pause before continuing
- name: Pause a Deployment.
  kubernetes.core.k8s:
    definition:
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: example
        namespace: testing
      spec:
        paused: True
    wait: yes
    wait_condition:
      type: Progressing
      status: Unknown
      reason: DeploymentPaused

# Patch existing namespace : add label
- name: add label to existing namespace
  kubernetes.core.k8s:
    state: patched
    kind: Namespace
    name: patch_namespace
    definition:
      metadata:
        labels:
          support: patch

# Create object using generateName
- name: create resource using name generated by the server
  kubernetes.core.k8s:
    state: present
    generate_name: pod-
    definition:
      apiVersion: v1
      kind: Pod
      spec:
        containers:
        - name: py
          image: python:3.7-alpine
          imagePullPolicy: IfNotPresent

# Server side apply
- name: Create configmap using server side apply
  kubernetes.core.k8s:
    namespace: testing
    definition:
      apiVersion: v1
      kind: ConfigMap
      metadata:
        name: my-configmap
    apply: yes
    server_side_apply:
      field_manager: ansible
```

---

This concludes part 2 of Hands-on Exercise #08.

Return to the [Exercise Page](../README.md)
