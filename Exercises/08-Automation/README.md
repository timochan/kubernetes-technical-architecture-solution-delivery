# Kubernetes - Technical Architecture Solution Delivery: Workflow Automantion
# Hands-on Exercises #08

### Objective



### Parts

[Part 1: Terraform](Part-01-Terraform.md)

[Part 2: Ansible](Part-02-Ansible.md)

Return to the course [Table of Content](../README.md)
