# Exercise #08 : Part 1
## Terraform

---

### Intro

"Automate Infrastructure on Any Cloud
Provision, change, and version resources on any environment."

---

### Terraform

Terraform needs to authenticate to the cluster.

The Terraform Kubernetes provider can get its configuration in two ways:

1. Explicitly by supplying attributes to the provider block. This includes:

- Using a kubeconfig file
- Supplying credentials
- Exec plugins

2. Implicitly through environment variables. This includes:

Using the in-cluster config

The provider always first tries to load a config file from a given location when config_path or config_paths (or their equivalent environment variables) are set. Depending on whether you have a current context set this may require config_context_auth_info and/or config_context_cluster and/or config_context.

We will use file config.


```yaml
provider "kubernetes" {
  config_path = "~/.kube/config"
}
```

---

##### Install terraform

We first need to install terraform.  Let us make sure we have the needed tools.  We should have them but just in case.

```bash
sudo NEEDRESTART_SUSPEND=a apt install -y gnupg software-properties-common
```

Add Hashicorp key to the local keyring.

```bash
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
clear
```

Verify the key, You should see "HashiCorp Security (HashiCorp Package Signing)"

```bash
gpg --no-default-keyring --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg --fingerprint
```

Add the Hashicorp package repo to your primary Lab instance.

```bash
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
```

And update the local package db

```bash
sudo apt update
```

Now we can install the terraform pacakage

```bash
sudo NEEDRESTART_SUSPEND=a apt install terraform
```

Verify it is installed

```bash
terraform version
```

---

The terraform files for this lab can be found in the following directory.

~~~shell
cd ~/content/kubernetes-technical-architecture-solution-delivery/src/08-Automation/terraform
~~~

---

### Review the content.

Terraform can have the content spread across different files ending in "".tf". When terraform runs it reads all the files to gather the resources, then it determines what changes need to be applied.  Terraform is a declarative system.  Let's look at the files we have in this project folder.

_providers.tf_
```
terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.0.0"
    }
  }
}
provider "kubernetes" {
  config_path = "~/.kube/config"
}
```

In the 'provider.tf' file we see that we are using the "kubernetes" provider and the config for that provider.

_namespace.tf_
```
resource "kubernetes_namespace" "test" {
  metadata {
    name = "nginx"
  }
}
```

In the 'namespaces.tf' file we see that we want to have a namespace called 'nginx'.

_deployments.tf_

```
resource "kubernetes_deployment" "test" {
  metadata {
    name      = "nginx"
    namespace = kubernetes_namespace.test.metadata.0.name
  }
  spec {
    replicas = 2
    selector {
      match_labels = {
        app = "MyTestApp"
      }
    }
    template {
      metadata {
        labels = {
          app = "MyTestApp"
        }
      }
      spec {
        container {
          image = "nginx"
          name  = "nginx-container"
          port {
            container_port = 80
          }
        }
      }
    }
  }
}
```

In the 'deployments.tf' file we see how a deployment is defined.

_services.tf_
```
resource "kubernetes_service" "test" {
  metadata {
    name      = "nginx"
    namespace = kubernetes_namespace.test.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.test.spec.0.template.0.metadata.0.labels.app
    }
    type = "NodePort"
    port {
      node_port   = 30201
      port        = 80
      target_port = 80
    }
  }
}
```

In the 'services.tf' file we see the service defined

---

##### Run terraform

Before we can apply this terraform project we need to first initialize it.

```bash
terraform init
```

It is a good idea to validate your code in your terraform project.

```bash
terraform validate
```

Terraform has a "plan" option which will show you what it plans to do when the project is applied.

```bash
terraform plan
```

Notice in the output "Plan: 3 to add, 0 to change, 0 to destroy.".  This is the summery of what will change, the details of the changes precede that line.

Before we apply it we can verify that the namespace does not exist.

```bash
kubectl get ns nginx
```

Now use terraform to create the resources

```bash
terraform apply
```

Now let us look for that namespace again.

```bash
kubectl get ns nginx
```

And view the resources in that namespace.

```bash
kubectl get all -n nginx
```

---

##### On your own

See if you can add an additional namespace called "apache" in that namespace add a deployment based on the Apache image, "httpd:latest".  Expose that image with a service type "NodePort".  Make sure you do not duplicate the NodePort number.

---

This concludes part 1 of Hands-on Exercise #08.  Continue on to Part 2 next.

[Part 2: Ansible](Part-02-Ansible.md)
