# Kubernetes - Technical Architecture Solution Delivery

### Lab Exercises

Here is a quick link to the [Exercise Page](Exercises)

### Overview
For IT professionals, developers, software engineers, and DevOps practitioners – technical architecture solution delivery training provides the technical practices and tooling fundamentals necessary to begin realizing the benefits of Kubernetes as a foundation for IT architecture, software engineering, and service/release delivery. The workshop includes many hands-on exercises that give you real-world practice on the engineering tools and skills a team needs in order to realistically implement your own flavor of Microservices architecture patterns so you can address the team needs of your own organization.  This course quickly teaches you the practical toolset and skills to get up and running with microservices in your own systems. Loosely coupled components and services allow teams to deploy more freely and independently, with less risk to the architecture.

### Learning Objectives
- Adopt, plan or improve your transition to Kubernetes
- Map technical practices to the business strategy behind Kubernetes
- Navigate different tools for enabling Kubernetes and how to use them and
enable more automated testing and self-service QA
- Communicate with stakeholders, management, and teams regarding needs and
expectations around Kubernetes
- Get hands-on practice with Docker, Kubernetes, and Jenkins tools for core
microservices architecture
- Get hands-on practice with the toolchain in our real-world application labs
- Build more mature DevOps practices through adoption
- Understand how to refactor monolithic systems into more modular, component- based systems
- Apply microservice use cases to continuous integration, delivery, and testing

### Target Audience
- Network engineers
- System and software architects
- Developers and engineers
- Testers and QA teams
- Release engineers
- IT operations staff
- Application Developers and Managers
- Operations Developers
- Technology Leaders & Managers
- Site reliability engineers
- DevOps practitioners and engineers
- DBAs and data engineering teams
- Information Security Pros

### Skill Level
Intermediate / Advanced

### Pre-Requisites
This is a hands-on coding and lab-intensive training course. Professionals who take this course should have some familiarity with engineering, containers, introductory Kubernetes, and basic cloud computing before attending. If you are an engineer interested to learn technical architectural solutions, this session is for you.

### Course Outline
#### __Part 1 : Container and Kubernetes Options__
1.	Alternative Container Management
  - Podman
  - Nerdctl
  - Buildah
2.	Kubernetes Distributions
3.	Exercise: Alternative Image Lab (Buildah, Podman, Nerdctl, etc)
4.	Exercise: K8s Distributions Lab (Kubeadm, Microk8s, RKE, K3s, etc)

#### __Part 2: Networking and Deployment Models__
1.	Ingress and Loadbalancer options
2.	Release Strategies
  - Blue / Green
  - Canary
3.	Deployment Control
  - Jobs / Cronjobs
  - Probes
  - Horizontal Pod Autoscalers
  - Cluster Autoscaling
4.	Exercise: Ingress and Load balancer Lab (MetalLB, Ingress, IngresClasses, etc)
5.	Exercise: Release Strategy Lab (Istio, Virtual Services, Canary, etc)
6.	Exercise: Deployment Control Lab (Job, Cronjobs, Probes, HPA, etc)

#### __Part 3: Kubernetes Infrastructure__
1.	Cluster Storage
  - Volumes
  - Persistent Storage
  - ConfigMaps and Secrets
2.	Backup Tools
3.	Automation Stack
  - Configuration and Provisioning (Terraform, Ansible, etc)
  - Continuous Delivery (GitOps, Argo, Helm, etc)
4.	Monitoring Stack
  - Cluster Logging
  - Cluster Metrics
  - Application Logging
  - Application Metrics
5.	Multi Cluster Considerations
6.	Exercise: Cluster Storage Lab (ConfigMaps, Storage Classes, Volumes, PVs, etc)
7.	Exercise: K8s Backup Lab (Velero, TrillioVault, etc)
8.	Exercise: Workflow Automation Lab (Terraform, Ansible, etc)
9.	Exercise: Monitoring Lab (EFK, Prometheus, Grafana, etc)

#### __Part 4: References__
1.	Official Project Documentation
2.	Organizations to follow
3.	Video Resources
4.	Meta Lists
